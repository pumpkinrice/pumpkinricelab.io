---
headless: true
---





<div class="content" id="content">
    {{- dict "Content" .Content "Ruby" $params.ruby "Fraction" $params.fraction "Fontawesome" $params.fontawesome | partial "function/content.html" | safeHTML -}}
    <!-- 添加文章版权声明信息 -->
    <div style="margin-top:2em;padding:0 0.5em;font-size:.875rem">
        <hr>
        <div style="padding-bottom:1em;">
            <p>本文作者：<a href="https://pumpkinrice.gitlab.io" target="_blank">三十的南瓜饭</a></p>
            <p>本文链接：<a href="{{ .Permalink }}" target="_blank">{{ .Title }}</a></p>
            <p>版权声明：<a href="https://creativecommons.org/licenses/by-nc/4.0/" target="_blank">「署名-非商业性使用-相同方式共享 4.0 国际」</a></p>
        </div>
    </div>
</div>
