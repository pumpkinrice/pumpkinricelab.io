---
title: {{ replace .TranslationBaseName "-" " " | title }}
author: 三十的南瓜饭
avatar: /me/Izumi-Kyouka.jpg
authorlink: https://pumpkinrice.gitlab.io
url: "{{ lower .Name }}.html"
copyright: true # 添加版权标识，如果不需要，改为 false
draft: true

#toc: false
categories: [ ]
tags: [ ]

# cover:
# images:

date: {{ .Date }}
lastmod: {{ .Date }}
# nolastmod: true

---




Cut out summary from your post content here.

<!--more-->

The remaining content of your post.



