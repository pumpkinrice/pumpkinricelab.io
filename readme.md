# 介绍

个人博客内容及配置

# 目录结构

```sh
▶ tree -L 2 .
.
├── config.toml # 配置
├── content # 文章源文件
│   ├── about # about 界面
│   └── posts # 文章内容
├── data # 数据
│   └── socials.toml # 社交媒体图标及链接
├── deploy.sh # 部署脚本
├── public # 生成的内容，这里不提交到 git
├── static # 静态内容，包括图片，css 等页面内容
│   ├── admin
│   ├── css
│   ├── icos
│   ├── img
│   └── me
└── themes # 主题
    ├── dream
    └── maupassant

15 directories, 3 files
```

# 更新记录
## 2023.07.12
在config->params中增加 `record` 标签，用于填写备案信息。
填写备案信息后会自动在 footer 标签中生成相应条目。
如果不需要备案信息，注释该条目即可。
