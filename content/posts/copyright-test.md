---
title: Copyright Test
author: 三十的南瓜饭
avatar: /me/Izumi-Kyouka.jpg
authorlink: https://pumpkinrice.gitlab.io
url: "copyright-test.html"
copyright: true # 添加版权标识，如果不需要，改为 false
draft: true

#toc: false
categories: [ ]
tags: [ ]

# cover:
# images:

date: 2023-07-10T08:39:33+08:00
lastmod: 2023-07-10T08:39:33+08:00
# nolastmod: true

---

Cut out summary from your post content here.

<!--more-->

The remaining content of your post.

