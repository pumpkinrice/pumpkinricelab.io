---
title: Hugo 启用 Twikoo 评论系统
author: 三十的南瓜饭
avatar: /me/Izumi-Kyouka.jpg
authorlink: https://pumpkinrice.gitlab.io
url: "hugo 启用 twikoo 评论系统.html"
slug: 4ab1a68d4f1bd1e37cab70e1c9661aei

#toc: false
categories: [ '工具箱' ]
tags: [ 'Hugo', 'Twikoo', '教程' ]
keywords: [ 'Hugo', 'Twikoo' ]

# cover:
# images:

copyright: true
draft: true
date: 2023-07-09T13:46:33+08:00
lastmod: 2023-07-09T13:46:33+08:00
# nolastmod: true
---

最近几天在折腾着搭建博客，目前主体已经基本完成了，开始准备评论系统和站内搜索、搜索引擎收录等内容。

这篇文章用来记录 Twikoo 评论系统的安装过程。

Twikoo 是个简洁、安全、免费的静态网站评论系统，是国内目前资料较多、社区较好的评论系统之一。

## Twikoo Server 安装

自建 Twikoo 评论系统私有部署
https://www.neko7ina.com/3pYN3eKz7vaZ1z.html

https://github.com/twikoojs/twikoo-docker

## 前端配置
![服务器在提交评论时的日志](http://pics.pumpkinrice.site:80/writing/2023/07/09/202307091858559.png)


## Qmsg实现即时通知

## 邮箱服务



<div class="content" id="content">
    <!-- 添加文章版权声明信息 -->
    <div style="margin-top:2em;padding:0 0.5em;font-size:.875rem">
        <hr>
        <div style="padding-bottom:1em;">
            <p>Author: <a href="https://pumpkinrice.gitlab.io" target="_blank">三十的南瓜饭</a></p>
            <p>Link: <a href="hugo 启用 twikoo 评论系统.html" target="_blank">Hugo 启用 Twikoo 评论系统</a></p>
            <p>Copyright Notice: All articles in this blog are licensed under <a href="https://creativecommons.org/licenses/by-nc/4.0/" target="_blank">「署名-非商业性使用-相同方式共享 4.0 国际」</a></p>
        </div>
    </div>
</div>


