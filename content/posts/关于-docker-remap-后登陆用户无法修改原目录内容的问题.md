---
title: 关于 docker remap 后登陆用户无法修改原目录内容的问题
author: 三十的南瓜饭
avatar: /me/Izumi-Kyouka.jpg
# authorlink: https://pumpkinrice.gitlab.io
slug: 4ab1a68d4f1bd1e37cab70e1c9661aee

# cover: /img/pexels-ylanite-koppens-796607.jpg
# images:
#   - /img/pexels-ylanite-koppens-796607.jpg

tags: [ 'Docker', 'LXC', 'cgroups/namespace' ]
categories: [ 'Linux操作系统' ]

data: 2023-07-07T02:17:43+08:00
lastmod: 2023-07-08T13:43:40+08:00

copyright: true
draft: false
---

这里和docker remap有关。设置了remap之后，可能有的容器原有用户会映射到`nobody`和`nogroup`，比如`nodejs`（对没错，也是折腾 hexo 的时候发现的。

这里先介绍问题以及解决问题，至于怎么开启 Docker 的 namespace 等之后整理好了再发出来。

## 情况复现
这里演示一下大概的情况：
先正常创建一个docker容器，让容器内应用使用 `1000:1000`的用户运行：
```bash
▶ docker run -it --name node-test -u 1000:1000 -v /opt/docker/hexo/www/blog:/var/www/blog -d node:latest           
bb9efcb959749b0e0c6dd5c9ec737595604b47d302294f77ed010a18a7e147a7
```

查看容器内内容：
```bash
# 先进入容器，查看 passwd 中的情况
▶ docker exec -it -u root node-test bash 
root@bb9efcb95974:/# grep node /etc/passwd
node:x:1000:1000::/home/node:/bin/bash
root@bb9efcb95974:/# grep nobody /etc/passwd
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin

# 查看 /home 目录权限
root@bb9efcb95974:/# ls -la /home
total 12
drwxr-xr-x 1 root   root    4096 Jul  4 17:28 .
drwxr-xr-x 1 root   root    4096 Jul  5 12:15 ..
drwxr-xr-x 2 nobody nogroup 4096 Jul  4 17:28 node

# 使用 root 在外部映射文件夹中创建内容
root@bb9efcb95974:/# echo "hello" > /var/www/blog/hello.txt

# 切换到 node，也创建一样的内容
root@bb9efcb95974:/# su node
node@bb9efcb95974:/$ echo "hello" > /var/www/blog/hello-node.txt

# 查看 blog 目录下的内容及权限
node@bb9efcb95974:/$ ls -la /var/www/blog/
total 24
drwxrwxrwx 3 node node 4096 Jul  5 12:20 .
drwxr-xr-x 3 root root 4096 Jul  5 12:15 ..
drwxr-xr-x 2 node node 4096 Jul  5 05:46 .github
-rw-r--r-- 1 node node   82 Jul  5 05:46 .gitignore
-rw-r--r-- 1 node node    6 Jul  5 12:20 hello-node.txt
-rw-r--r-- 1 root root    6 Jul  5 12:20 hello.txt

# 尝试使用 root 删除该目录无效
root@bb9efcb95974:/home# rm -rf ./node/
rm: cannot remove './node/.profile': Permission denied
rm: cannot remove './node/.bash_logout': Permission denied
rm: cannot remove './node/.bashrc': Permission denied
```

退出容器之后查看宿主机中对应目录的内容：

```bash    
▶ la /opt/docker/hexo/www/blog 
total 16K
drwxr-xr-x 2 admin  admin  4.0K Jul  5 13:46 .github
-rw-r--r-- 1 admin  admin    82 Jul  5 13:46 .gitignore
-rw-r--r-- 1 admin  admin     6 Jul  5 20:20 hello-node.txt
-rw-r--r-- 1 427680 427680    6 Jul  5 20:20 hello.txt
        
▶ id admin
uid=1000(admin) gid=1000(admin) groups=1000(admin)
```

通过上述操作，可以知道：
- 宿主机中 uid 为 1000 的用户（这里是`admin`）确实映射到了容器中（`node`）
- 权限配置正确，除其 node 外，其他用户权限均被映射到了`427680+`
- `/home/node`目录权限错误，原属于 `node` 的目录被映射到了 `nobody` 上
- 通过容器内的 `root` 用户甚至无法删除该目录

```bash                                        
# 使用 host 创建一个
▶ docker run -it --name node-test2 --userns=host -u 1000:1000 -v /opt/docker/hexo/www/blog:/var/www/blog -d node:latest  
fdf986e76bd32bdfb85eb7a15d59cec030a9e25fe440011ec5143391b020a0d4
                                               
▶ docker exec -it -u root node-test2 bash 
root@fdf986e76bd3:/# ls -la /home
total 12
drwxr-xr-x 1 427680 427680 4096 Jul  4 17:28 .
drwxr-xr-x 1 427680 427680 4096 Jul  5 12:43 ..
drwxr-xr-x 2 428680 428680 4096 Jul  4 17:28 node
```
从这里就可以看到，限制之后，原本的 `/home/node` 目录其实是映射到了`428680`上，即 `root+1000`。root 为 427680，+1000就映射成了428680。

## 问题解决

这时候就靠宿主机来解决了，解决方法主要是将需要修改的目录拷贝出来，修改权限之后再复制回去。这是目前找到的最简单的方法，docker有没有直接从宿主机修改容器内容的方法还不清楚。

先使用`docker cp`将目录拷贝出来：
```bash
docker cp hexo_node:/home/node .
```

使用宿主机中的 root 用户修改属主：
```bash
chown -R 1000:1000 ./node/
```

最后将修改后的目录复制回去：
```bash
docker cp ./node/ hexo_node:/home/node
```

完成之后在容器中确认一下：
```shell
root@fdf986e76bd3:/# ls -la /home/node/  
total 28
drwxr-xr-x 1 node node 4096 Jul  5 12:12 .
drwxr-xr-x 1 root root 4096 Jul  4 17:28 ..
-rw------- 1 node node   24 Jul  5 12:12 .bash_history
-rw-r--r-- 1 node node  220 Apr 23 21:23 .bash_logout
-rw-r--r-- 1 node node 3526 Apr 23 21:23 .bashrc
-rw-r--r-- 1 node node  807 Apr 23 21:23 .profile
```

这样就修改完成了。

