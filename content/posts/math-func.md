---
title: Math Func
author: 三十的南瓜饭
avatar: /me/Izumi-Kyouka.jpg
authorlink: https://pumpkinrice.gitlab.io
url: "math-func.html"
copyright: true # 添加版权标识，如果不需要，改为 false
draft: true

#toc: false
categories: [ "math" ]
tags: [ "functions" ]

# cover:
# images:

date: 2023-07-12T14:14:25+08:00
lastmod: 2023-07-12T14:14:25+08:00
# nolastmod: true

---

# 连续性
**定义一：**
设 $y = f(x)$ 在点 $x_0)$ 的某邻域内有定义，若：
$$
\lim_{\Delta x \rightarrow 0}{\Delta y} = \lim_{\Delta x \rightarrow 0} \left[ f(x_0 + \Delta x) - f(x_0) \right] = 0
$$
则称 $y=f(x)$在点$x_0$处连续。

**定义二：**
设 $y = f(x)$ 在点 $x_0)$ 的某邻域内有定义，若：
$$
\lim_{\Delta x \rightarrow 0}{f(x)} = f(x_0) 
$$
则称 $y=f(x)$在点$x_0$处连续。

> 定义一 & 定义二等价。



