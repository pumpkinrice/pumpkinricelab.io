---
title: 关于本站图片不能显示的问题
author: 三十的南瓜饭
avatar: /me/Izumi-Kyouka.jpg
authorlink: https://pumpkinrice.gitlab.io

slug: 4ab1a68d4f1bd1e37cab70e1c9661ae1

categories: [ '本站' ]
tags: [ '杂项' ]

# cover: 
# images:

data: 2023-07-07T09:10:49+08:00
lastmod: 2023-07-08T13:43:40+08:00
draft: false

---

目前图床已经在内网服务器搭好了，但是还不能在公网使用，等之后配置好了会逐步上线。

之后也会把自己搭建私人图床的过程整理发布出来。

