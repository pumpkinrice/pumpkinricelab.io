---
title: OpenWRT 使用 ADGuardHome 实现广告过滤
author: 三十的南瓜饭
avatar: /me/Izumi-Kyouka.jpg
# authorlink: https://pumpkinrice.gitlab.io
slug: 4ab1a68d4f1bd1e37cab70e1c9661aej

# cover: /img/
# images:
#   - /img/

tags: ['OpenWRT', 'ADBlock', 'DNS Server']
categories: [ '工具箱' ]

date: 2023-07-07T02:21:18+08:00
lastmod: 2023-07-08T13:43:40+08:00
draft: false
copyright: true

---


现在路由器使用的电犀牛R68s，系统是OpenWRT。这个配置只用来作路由器性能过剩了，就想着在上面弄些其它服务。

先想到的是去广告。查了一圈，发现资料比较多，同时大家也比较推荐的广告过滤服务是 AdGuard Home，那么就折腾一下咯。

## ADGuard Home 安装
OpenWRT 上面可以找到AdGuard Home的ipk包，如果找不到，可以从openwrt镜像站下载对应安装包进行安装。

- 自动安装ADG：
	```bash
	opkg install adguardhome
	```

- 使用ipk安装：
	首先需要找到自己的openwrt版本，再去下载对应的软件包：
	ipk文件可以在immortalwrt上找到（<font size=4 color=#f90000>注意自己的OpenWRT版本和架构！</font>）：https://downloads.immortalwrt.org/releases/21.02.1/packages/aarch64_generic/
	```bash
	wget [adg.ipk path] # 下载ipk
	opkg install ./adguardhome*.ipk # 本地安装
	```

## 启动脚本修改
但是我下载的版本安装之后存在一些问题，需要修改`/etc/init.d/adguardhome`文件，不然无法正常启动。修改之后的文件如下：
```vim
root@OpenWrt:~# cat /etc/init.d/adguardhome 
#!/bin/sh /etc/rc.common

#PROG=/usr/bin/AdGuardHome
PROG=AdGuardHome

USE_PROCD=1

# starts just after network starts to avoid some network race conditions
START=25
# stops before networking stops
STOP=89

start_service() {
  config_load adguardhome
  config_get_bool enabled config enabled
  config_get WORK_DIR config workdir

# 这一行用于判断 adguardhome 是否已经在运行，但是如果启用的话会导致 start/restart 失败，所以注释掉
#  [ "$enabled" -eq "1" ] || return 1
  [ -d "$WORK_DIR" ] || mkdir -m 0755 -p "$WORK_DIR"
 

  procd_open_instance
  procd_set_param command "$PROG" -c /etc/adguardhome.yaml -w "$WORK_DIR" --no-check-update
  procd_set_param command "$PROG" -c /etc/adguardhome.yaml # -w /tmp/adguardhome --no-check-update
  procd_set_param stdout 1
  procd_set_param stderr 1

echo "hello" # echo 只是查看程序是否运行到这里，没有实际作用
  procd_close_instance
}
```

## 服务文件配置
adguardhome 服务启动之后就可以通过`3000`（默认端口，如果该端口已经被占用，则需要重新指定）端口访问其配置界面，初始化需要配置的内容主要是界面管理端口、DNS服务端口。
![](http://pics.pumpkinrice.site:80/writing/2023/07/06/202307062045500.png)

管理端口一般设置为3000即可，DNS服务端口则需要根据实际情况设置，默认为`53` 。如果adguardhome服务安装在路由器上，因为路由器本身可能自带了dns服务，所以需要设置其他没有被占用的端口。
![](http://pics.pumpkinrice.site:80/writing/2023/07/06/202307062045244.png)

设置完成之后 adguard 相关配置文件会在 `/etc/adguardhome.yaml` 里面看到：
```vim
root@OpenWrt:/etc# cat adguardhome.yaml 
bind_host: 192.168.100.1
bind_port: 3000
users:
  - name: [name]
    password: [password]
auth_attempts: 5
block_auth_min: 15
http_proxy: ""
language: ""
theme: auto
debug_pprof: false
web_session_ttl: 720
```

如果需要重新指定网页端口，只需要更改 `adguardhome.yaml` 里面`bind_port`就可以了，修改之后需要重新运行adg：`/etc/init.d/adguardhome restart`。

## DNS 服务器设置

关于DNS作用，这位up讲解比较详细了，这里直接贴出来：
> 看到大家对这两个DNS地址设定有很多疑惑，那么补充一点把原理说一下，首先要知道，DNS就是域名解析，找到你要访问的域名对应的互联网真实地址，互联网架构决定了，服务器离我们越近、转发的次数越少速度就越快，（当然这里要排除ISP之间的互联策略，有可能电信联通同在一个机房里但两台DNS的PING值也很高的，）我们自架DNS是为了屏蔽广告，但运营商如果给你的本地DNS本来就是很纯净的，那么就没有必要舍近求远去用别人家的DNS，在AdGuardHome里上游DNS和Bootstrap DNS都设定成本地的DNS速度就是最快的，但，运营商本地区域性的DNS对域名记录是偏少的，知名网站肯定都有记录，当某个域名不知名并没有记录时，你的网页就在那里转圈圈半天打不开，再但，国内的某些运营商你懂的，比如我以前用过广电的网络，解析电信线路就卡得一批，还有欢迎的弹窗广告，同一运营商，不同省份，不同城市，DNS也不一定一样，这个得大家自己在平时多用心观察。所以我们就要用阿里这些知名的DNS，不仅速度快且记录全，具体到个人的网络环境就又不一样的，因此最好的办法就是CMD里去ping，找到最优秀而且没广告的就可以了。最后还是建议大家用公共的DNS，虽然比你本地的慢一点，但这点速度肯定是你体会不到的，再者，玩这论坛的都是到处找教程、学习资料，别人的资料可能就在自建的路由器上，国外申请免费域名，你本地运营商的DNS肯定没有记录的，这个见仁见智哈，明白了这个道理就按个人需求来吧。
——[来吧，AdGuardHome去广告和DNS正确姿势](https://www.right.com.cn/forum/thread-4090928-1-1.html)

AdGuard Home 配置完成后需要重新设置一下OpenWRT的DNS服务器端口。

在OpenWRT 管理界面->网络->DHCP/DNS->高级设置下面有个 `DNS服务器端口`这个是OpenWRT接受下面连接的设备DNS查询用的端口，<font size=4 color=#ff0000>不能修改!</font>
在管理界面->网络->DHCP/DNS->常规设置里面有`DNS 转发`这个条目，这里是指定路由器上DNS服务器的监听端口，默认为`127.0.0.1:5333`。
也就是说当一个设备提交DNS解析的过程是这样的：设备提交域名解析请求->OpenWRT/路由器:53接受解析请求->路由器转发请求到DNS服务器(127.0.0.1:5333)->DNS服务器进行解析->*上游DNS服务器查询*->向设备返回查询到的域名/IP。DNS服务器如果在本级没有找到对应域名，则又会向设置的上游DNS服务器提交查询请求，直到返回查询结果为止（成功/失败）。
![](http://pics.pumpkinrice.site:80/writing/2023/07/06/202307062044960.png)

这里直接设置该端口为`127.0.0.1:5300`，即之前在adguardhome里面配置的监听端口。

在adguardhome里面还需要设置一下上游服务器地址，自带那个地址好像存在一点问题（我这里使用没法解析域名）。
![](http://pics.pumpkinrice.site:80/writing/2023/07/06/202307062044176.png)
openwrt自带的dns服务器配置，在`/var/run/dnscache/dnscache.conf`：
```vim
root@OpenWrt:~# cat /var/run/dnscache/dnscache.conf 
global {
    perm_cache=1024;        # dns缓存大小，单位KB，建议不要写的太大
    cache_dir="/var/dnscache";     # 缓存文件的位置
    pid_file = /var/run/dnscache.pid;
    server_ip = 127.0.0.1;        # pdnsd监听的网卡，0.0.0.0是全部网卡
    server_port=5333;           # pdnsd监听的端口，不要和别的服务冲突即可
    status_ctl = on;
    paranoid=on;                  # 二次请求模式，如果请求主DNS服务器返回的是垃圾地址，就向备用服务器请求
    query_method=udp_only;
    neg_domain_pol = off;
    par_queries = 400;          # 最多同时请求数
    min_ttl = 1h;               # DNS结果最短缓存时间
    max_ttl = 1w;               # DNS结果最长缓存时间
    timeout = 10;               # DNS请求超时时间，单位秒
}

server {
    label = "routine";
    ip = 114.114.114.114,114.114.115.115,223.5.5.5,223.6.6.6,180.76.76.76,119.29.29.29,119.28.28.28,1.2.4.8,210.2.4.8;     # 这里为主要上级 dns 的 ip 地址，建议填写一个当地最快的DNS地址
    timeout = 5;              # DNS请求超时时间
    ... # 这里省略脏ip
    reject_policy = fail;
}

server {
    label = "special";                  # 这个随便写
    ip = 117.50.10.10,52.80.52.52,119.29.29.29; # 这里为备用DNS服务器的 ip 地址
    port = 5353;                        # 推荐使用53以外的端口（DNS服务器必须支持
    proxy_only = on;
    timeout = 5;
}

source {
	owner=localhost;
//	serve_aliases=on;
	file="/etc/hosts";
}

rr {
	name=localhost;
	reverse=on;
	a=127.0.0.1;
	owner=localhost;
	soa=localhost,root.localhost,42,86400,900,86400,86400;
}
```

这里我直接把OpenWRT的DNS上游服务器地址复制过来：
![](http://pics.pumpkinrice.site:80/writing/2023/07/06/202307062044860.png)
```vim
114.114.114.114
114.114.115.115
223.5.5.5
223.6.6.6
180.76.76.76
119.29.29.29
119.28.28.28
210.2.4.8
```

如果没有其他要求的话，这里DNS服务器配置即完成。

如果有多个dns服务器可以直接在这些设备上指定adg（如果没有可以不用配置）
adguardhome 提供了各个客户端的dns服务器配置方式：
![](http://pics.pumpkinrice.site:80/writing/2023/07/06/202307062044944.png)

通过这些 DNS 配置，再加上一些广告屏蔽，就可以愉快上网了。

## 广告屏蔽
ADG的广告屏蔽教程有很多，只需要点点点即可，这里不做介绍了。

## 服务屏蔽
ADG也提供了相应的服务屏蔽，在`过滤器->已阻止的服务`可以找到。开启相应开关之后，对应的服务变不能再使用了。
![](http://pics.pumpkinrice.site:80/writing/2023/07/06/202307062044808.png)

> 客户端设置的部分中已提到过阻止的服务。此功能允许用户快速有效地阻止特定流行网站和服务器。虽然可以通过客户端设置将设置指定为特定的设备，但在过滤器标签中，你可以将全部设置都同时应用于所有链接网络的设备上。
> 
> 请注意：个人的客户端设置比常规阻止服务器设置有优先权。
> ——[ADG的介绍](https://adguard.com/zh_cn/blog/in-depth-review-adguard-home.html#filters)

假如这里阻止用了bilibili，则相应的域名会被解析为其他不可用地址（但是测试知乎好像不行27/06/2023 12:16）：
```bash
# 阻止之后                
▶ ping bilibili.com
PING bilibili.com (127.0.0.1) 56(84) 字节的数据。
64 字节，来自 localhost (127.0.0.1): icmp_seq=1 ttl=64 时间=0.020 毫秒
64 字节，来自 localhost (127.0.0.1): icmp_seq=2 ttl=64 时间=0.058 毫秒
64 字节，来自 localhost (127.0.0.1): icmp_seq=3 ttl=64 时间=0.057 毫秒
64 字节，来自 localhost (127.0.0.1): icmp_seq=4 ttl=64 时间=0.068 毫秒
64 字节，来自 localhost (127.0.0.1): icmp_seq=5 ttl=64 时间=0.047 毫秒
^C
--- bilibili.com ping 统计 ---
已发送 5 个包， 已接收 5 个包, 0% packet loss, time 4042ms
rtt min/avg/max/mdev = 0.020/0.050/0.068/0.016 ms

# 阻止关闭
▶ ping bilibili.com
PING bilibili.com (120.92.78.97) 56(84) 字节的数据。
64 字节，来自 120.92.78.97 (120.92.78.97): icmp_seq=1 ttl=53 时间=43.5 毫秒
64 字节，来自 120.92.78.97 (120.92.78.97): icmp_seq=2 ttl=53 时间=43.2 毫秒
64 字节，来自 120.92.78.97 (120.92.78.97): icmp_seq=3 ttl=53 时间=43.5 毫秒
64 字节，来自 120.92.78.97 (120.92.78.97): icmp_seq=4 ttl=53 时间=43.5 毫秒
64 字节，来自 120.92.78.97 (120.92.78.97): icmp_seq=5 ttl=53 时间=43.2 毫秒
64 字节，来自 120.92.78.97 (120.92.78.97): icmp_seq=6 ttl=53 时间=43.6 毫秒
^C
--- bilibili.com ping 统计 ---
已发送 6 个包， 已接收 6 个包, 0% packet loss, time 5113ms
rtt min/avg/max/mdev = 43.221/43.424/43.614/0.147 ms

```

这个功能可以让我们禁用掉某些自己不喜欢的网站。

## 配置文件备份
ADG 的配置内容保存在 `/etc/adguardhome.yaml`中，只需要将该文件进行备份即可。

## 密码重置
ADG 没有提供重新设置帐号或者重设密码的操作，但是在登陆界面的提供了重设密码的方法：点击忘记密码即可看到。
官方提供的方法需要 htpasswd，是 apache 的工具之一，这里用不到apache，所以用网页提供的计算器就好：
https://www.jisuan.mobi/p163u3BN66Hm6JWx.html
![](http://pics.pumpkinrice.site:80/writing/2023/07/06/202307062043775.png)

输入明文，点击生成即可。然后复制密码，粘贴到 `/etc/adguardhome.yaml`中password之后就可以了：
![](http://pics.pumpkinrice.site:80/writing/2023/07/06/202307062043970.png)

登陆的时候用户名和密码都是 admin。
